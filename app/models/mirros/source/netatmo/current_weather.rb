# frozen_string_literal: true

module Mirros
  module Source
    module Netatmo
      class CurrentWeather < ::GroupSchemas::CurrentWeather
        self.table_name = 'group_schemas_current_weathers'
      end
    end
  end
end
