# frozen_string_literal: true

module Mirros
  module Source
    module Netatmo
      class ApplicationController < ActionController::API
        def callback
          Rails.logger.debug params

          render json: {}, status: status
        end
      end
    end
  end
end
