# frozen_string_literal: true

module Mirros
  module Source
    module Netatmo
      class Engine < ::Rails::Engine
        isolate_namespace Mirros::Source::Netatmo
        config.generators.api_only = true
      end
    end
  end
end
