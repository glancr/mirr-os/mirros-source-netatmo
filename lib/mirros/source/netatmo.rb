# frozen_string_literal: true

require 'mirros/source/netatmo/engine'
require 'netatmo'

module Mirros
  module Source
    module Netatmo
      class Hooks
        REFRESH_INTERVAL = '5m'

        # @return [String]
        def self.refresh_interval
          REFRESH_INTERVAL
        end

        # @param [Hash] configuration
        def initialize(instance_id, configuration)
          @instance_id = instance_id
          @configuration = configuration

          @client = ::Netatmo::Client.new do |config|
            config.client_id = @configuration['client_id']
            config.client_secret = @configuration['client_secret']
            config.username = @configuration['email']
            config.password = @configuration['password']
          end
        end

        def default_title
          (@configuration['email']).to_s
        end

        def configuration_valid?
          config = @client.config

          config.email && config.password && config.client_id && config.client_secret
        end

        def validate_configuration
          # TODO
          true
        end

        def list_sub_resources
          @client.get_station_data.devices.map do |device|
            [device.id, "#{device.name} (#{default_title})"]
          end
        end

        def fetch_data(group, sub_resources)
          records = []

          case group
          when 'current_weather'
            devices = @client.get_station_data.devices.select { |device| sub_resources.include?(device.id) }

            devices.each do |device|
              location = device.place&.location&.first&.data.try(:[], 'address').try(:[], 'village')

              current_weather = Mirros::Source::Netatmo::CurrentWeather.find_or_initialize_by(id: device.id)

              current_weather.update(
                station_name: device.name,
                location: location
              )

              outdoor_module = device.outdoor_module
              rain_gauge     = device.rain_gauge
              wind_gauge     = device.wind_gauge

              entry = current_weather.entries.find_or_initialize_by(uid: device.last_status_store.to_s)

              rain_last_hour = rain_gauge&.rain&.sum_rain_1.to_i
              condition_code = rain_last_hour.positive? ? '10d' : '01d'

              entry.update(
                temperature: outdoor_module&.temperature&.value,
                humidity: outdoor_module&.humidity&.value,
                air_pressure: outdoor_module&.pressure&.value,
                wind_speed: wind_gauge&.wind&.wind_strength,
                wind_angle: wind_gauge&.wind&.wind_angle,
                rain_last_hour: rain_last_hour,
                condition_code: condition_code
              )

              # We only need the latest record, as the current_weather schema does not have a history.
              current_weather.entries.each { |e| e.mark_for_destruction unless e.uid.eql?(entry.uid) }

              records << current_weather
            end
          end

          records
        end
      end
    end
  end
end
