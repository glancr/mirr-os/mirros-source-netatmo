# frozen_string_literal: true

module Mirros
  module Source
    Netatmo::Engine.routes.draw do
      get 'auth-callback', to: 'application#callback'
    end
  end
end
