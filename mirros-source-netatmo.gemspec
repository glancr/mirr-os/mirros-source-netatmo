# frozen_string_literal: true

require 'json'
$LOAD_PATH.push File.expand_path('lib', __dir__)

require 'mirros/source/netatmo/version'

Gem::Specification.new do |spec|
  spec.name        = 'mirros-source-netatmo'
  spec.version     = Mirros::Source::Netatmo::VERSION
  spec.authors     = ['Marco Roth', 'Tobias Grasse']
  spec.email       = ['marco.roth@intergga.ch', 'tg@glancr.de']
  spec.homepage    = 'https://gitlab.com/glancr/core-team/source--netatmo'
  spec.summary     = 'mirr.OS data source for weather data from Netatmo'
  spec.description = 'Fetches current weather data from Netatmo'
  spec.license     = 'MIT'
  spec.metadata    = {
    'json' => {
      type: 'sources',
      title: {
        enGb: 'Netatmo',
        deDe: 'Netatmo'
      },
      description: {
        enGb: spec.description,
        deDe: spec.description,
        frFr: spec.description,
        esEs: spec.description,
        plPl: spec.description,
        koKr: spec.description
      },
      groups: ['current_weather'],
      compatibility: '0.13.0'
    }.to_json
  }

  spec.files = Dir['{app,config,db,lib}/**/*', 'LICENSE', 'Rakefile', 'README.md']

  spec.add_dependency 'lhc'
  spec.add_dependency 'netatmo', '~> 0.1.2'

  spec.add_development_dependency 'rails'
  spec.add_development_dependency 'rubocop', '~> 0.87'
  spec.add_development_dependency 'rubocop-rails'
end
